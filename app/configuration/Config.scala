package configuration

import com.typesafe.config.ConfigFactory


object Config {
  val XIVOCTI_HOST = ConfigFactory.load().getString("xivocti.host")
  val XIVOCTI_PORT = ConfigFactory.load().getString("xivocti.port")
}