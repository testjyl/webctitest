package xivo.network


import akka.actor._
import scala.concurrent.duration._
import java.net.InetSocketAddress
import akka.util.{ ByteString, ByteStringBuilder }
import play.api._
import org.xivo.cti.message.CtiMessage
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.json.JsValue
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.iteratee.Enumerator

class CtiLink(host: String, port: Int) extends Actor {
  import IO._

  var handle: Option[SocketHandle] = None
  var respondTo: Option[ActorRef] = None
  var bchannel: Option[Channel[JsValue]] = None

  var state = IO.IterateeRef.sync()

  def receive = {
    case Read(socket, bytes) =>
      state(IO Chunk bytes)
      for {
        _ <- state
        received <- IO takeUntil ByteString("\n")
      } yield {
        respondTo.foreach { _ ! received }
      }

    case Connected(h, _) =>
      handle = Some(h)
      Logger("CtiLink").debug("connected")
      respondTo.foreach { _ ! "start" }

    case Attach(r,browserChannel) =>
      Logger("CtiLink").debug("Starting network connection")
      respondTo = Some(r)
      bchannel = Some(browserChannel)
      state = IO.IterateeRef.sync()
      IOManager(context.system).connect(new InetSocketAddress(host, port))

    case Closed(socket, cause) =>
      Logger("CtiLink").debug("Connection closed")

    case Detach =>
      Logger("CtiLink").debug("requesting end of connection")
      handle.foreach { _.close }; handle = None

    case s: String =>
      Logger("CtiLink").debug(">>>>> :" + s)
      handle.foreach { _ write ByteString("%s\n".format(s)) }

    case message : CtiMessage =>
      Logger("CtiLink").debug("login step did not used it !! " + message.toString())
      sendMessage("kind", "user", message.toString())

    case LoggedIn =>
      Logger("CtiLink").debug("user Logged in")
      sendMessage("kins", "user", "loggedin")

    case Talk(username,text) =>
      Logger("CtiLink").debug("received from browser " + text)
      self ! text
    case Quit(username) =>
      Logger("CtiLink").debug("web socket terminated for username " + username)
      sender ! Detach
  }
    def sendMessage(kind: String, user: String, text: String) {
    val msg = JsObject(
      Seq(
        "kind" -> JsString(kind),
        "user" -> JsString(user),
        "message" -> JsString(text)
      )
    )
    bchannel.foreach(_.push(msg))
  }

}

case class Attach(respondTo: ActorRef,browserChannel: Channel[JsValue])
case object Detach
case class Talk(username: String, text: String)
case class Quit(username: String)