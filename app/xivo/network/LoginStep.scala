package xivo.network

import akka.actor._
import akka.util.{ ByteString, ByteStringBuilder }
import play.api._
import org.xivo.cti.MessageParser
import org.xivo.cti.message.LoginAck
import org.xivo.cti.message.CtiMessage
import org.xivo.cti.message.LoginPassAck
import org.xivo.cti.MessageFactory
import org.json.JSONObject

case class LoginStep(val username: String, val password: String) extends Actor {
  val messageFactory: MessageFactory = new MessageFactory()
  val messageParser: MessageParser = new MessageParser()
  val identity = "scalatest"

  def receive = {
    case "start" =>
      Logger("LoginStep").debug("start requested user : " + username + " password " + password)
      sendLogin

    case bytes: ByteString =>
      Logger("LoginStep").debug("<<<<<<:" + bytes.decodeString("US-ASCII"))
      val message = messageParser.parse(new JSONObject(bytes.decodeString("US-ASCII")))
      Logger("LoginStep").debug(message.getClass().toString())
      processMessage(message)
  }

  def processMessage(message: CtiMessage) = message match {
    case message: LoginAck =>
      Logger("LoginStep").debug("***** login ack received : session Id " + message.sesssionId)
      sendLoginPass(message.sesssionId)
    case message: LoginPassAck =>
      Logger("LoginStep").debug("***** login pass ack received")
      sendLoginCapa(message.capalist.get(0))
      sender ! LoggedIn
    case _ =>
      Logger.debug("unprocessed message type " + message.getClass().toString())
      sender ! message

  }
  def sendLogin = {
    Logger("LoginStep").debug("sending login " + username)
    val message = messageFactory.createLoginId(username, identity)
    sender ! message.toString()
  }

  def sendLoginPass(sessionId: String) = {
    Logger("LoginStep").debug("sending loginPass " + password + " session " + sessionId)
    val message = messageFactory.createLoginPass(password, sessionId)
    sender ! message.toString()
  }

  def sendLoginCapa(capaId: Int) = {
    val message = messageFactory.createLoginCapas(capaId)
    sender ! message.toString()
  }
}

case class LoggedIn()
