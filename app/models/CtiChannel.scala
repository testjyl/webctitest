package models

import akka.actor._
import scala.concurrent.duration._
import play.api._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.concurrent._
import akka.util.Timeout
import akka.pattern.ask
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import xivo.network.CtiLink
import xivo.network.LoginStep
import xivo.network.Attach
import akka.actor.EmptyLocalActorRef
import xivo.network.Detach
import xivo.network.LoggedIn
import configuration.Config
import xivo.network.Talk
import xivo.network.Quit

object CtiChannel {

  implicit val timeout = Timeout(1 second)

  lazy val default = {
    {
      val ctiChannelActor = Akka.system.actorOf(Props[CtiChannel])
      ctiChannelActor
    }
  }

  def join(username: String, password: String): scala.concurrent.Future[(Iteratee[JsValue, _], Enumerator[JsValue])] = {

    Logger.debug("join : " + username + " " + password)

    (default ? Join(username, password)).map {

      case Connected(inChannel,toBrowserChannel) =>

        Logger.debug("user " + username + " login to cti server in progress")

        (inChannel, toBrowserChannel)

    }
  }
}

class CtiChannel extends Actor {

  val (ctiEnumerator, ctiChannel) = Concurrent.broadcast[JsValue]
  val system = ActorSystem()

  def createActors(username: String, password: String): (ActorRef, ActorRef) = {
    if (system.actorFor("/user/ctiLink_" + username).isTerminated)
      (system.actorOf(Props(new CtiLink(Config.XIVOCTI_HOST, Config.XIVOCTI_PORT.toInt)), name = "ctiLink_" + username),
        system.actorOf(Props(new LoginStep(username, password)), name = "loginstep_" + username))
    else
      (system.actorFor("/user/ctiLink_" + username), system.actorFor("/user/loginstep_" + username))
  }

  def receive = {

    case Join(username, password) => {
      Logger("CtiChannel").debug("Connection in progress for user : " + username)

      val (ctiLink, loginStep) = createActors(username, password)
      val (browserEnumerator, tobrowserChannel) = Concurrent.broadcast[JsValue]
      val fromBrowserChannel = Iteratee.foreach[JsValue] { event =>
          ctiLink ! Talk(username, (event \ "text").as[String])
        }.mapDone { _ =>
          ctiLink ! Quit(username)
        }

      ctiLink ! Detach
      ctiLink ! Attach(loginStep, tobrowserChannel)
      sender ! Connected(fromBrowserChannel,browserEnumerator)

    }
  }

}
case class Connected(iteratee: Iteratee[JsValue,Unit],enumerator: Enumerator[JsValue])
case class Join(username: String, password: String)

