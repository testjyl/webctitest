package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json._
import models.CtiChannel
import models.Join

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def login = Action {
    Ok(views.html.login(loginForm))
  }

  def doLogin = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => // binding failure, you retrieve the form containing errors,
        BadRequest(views.html.login(formWithErrors)),
      value => { // binding success, you get the actual value
        val (username, password) = value
        Ok(views.html.ctichannel(username, password))
      })
  }

  def ctiChannel(username: String, password: String) = WebSocket.async[JsValue] { request =>

    CtiChannel.join(username,password)
    
  }

  val loginForm = Form(
    tuple(
      "username" -> nonEmptyText,
      "password" -> text))

}