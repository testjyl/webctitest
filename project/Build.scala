import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "webctitest"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    "org.xivo" % "cticlient" % "0.0.1-SNAPSHOT",
    jdbc,
    anorm
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers += ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository")
  )

}
